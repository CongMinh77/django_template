# django_template

### Create virtual environment

```
python3 -m venv django_template

source django_template/bin/activate
```

### Install Django

```
python3 -m pip install Django
```
